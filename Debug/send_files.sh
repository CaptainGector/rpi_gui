#!/bin/bash

cd ../

scp src/main.c gector@raspberrypi.local:~/lab8/src/
scp src/msgQueue.c gector@raspberrypi.local:~/lab8/src/
scp src/msgQueue.h gector@raspberrypi.local:~/lab8/src/

scp assets/main.glade gector@raspberrypi.local:~/lab8/assets/