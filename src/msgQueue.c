/*
 * msgQueue.c
 *
 *  Created on: Nov 24, 2020
 *      Author: Zachary Whitlock
 *
 * Writes to the GUI message queue and reads from the client message queue
 */

#include "msgQueue.h"


// Open the message queue
// Make the receive functionality non-blocking.
// Set 'client' to anything except 0 to make this the client.
// Returns -1 if error, anything >= 0 is good.
int createMsgQueues(int client) {
    struct mq_attr attr;

    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_MESSAGES;
    attr.mq_msgsize = MAX_MSG_SIZE;
    attr.mq_curmsgs = 0;

    if ((client_server = mq_open (CLIENT_MSG_FILE, O_RDWR | O_CREAT | O_NONBLOCK, QUEUE_PERMISSIONS, &attr)) == -1) {
    	perror("Error on opening client file: ");
    	return -1;
    }

    if ((other_server = mq_open (OTHER_MSG_FILE, O_RDWR | O_CREAT | O_NONBLOCK, QUEUE_PERMISSIONS, &attr)) == -1) {
    	perror("Error on opening GUI file:");
    	return -1;
    }

    if (client != 0){
    	mqd_t tempHolder = client_server;
    	client_server = other_server;
    	other_server = tempHolder;
    	printf("Initialized queues. Sending to '%s' and receiving from '%s'\n",
    			OTHER_MSG_FILE,
				CLIENT_MSG_FILE);
    } else {
    	printf("Initialized queues. Sending to '%s' and receiving from '%s'\n",
    			CLIENT_MSG_FILE,
				OTHER_MSG_FILE);
    }

    return 0;
}

// Send a message to client program
// Returns -1 if error, anything >= 0 is good.
int sendToOther(const char *msgBuf, size_t bufSize) {
	if (mq_send(client_server, msgBuf, bufSize, 0) < 0) {
		perror("Error sending message.");
		return -1;
	}
	return 0;
}

// Close a message queue
// Returns -1 if error, anything >= 0 is good.
// TODO
int closeQueues() {
	mq_close(other_server);
	mq_close(client_server);
	return 0;
}

// Attempt to receive a message from the queue
// Returns -1 if error, anything >= 0 is good.
int readFromOther(char *msgBuf, size_t bufSize) {
	ssize_t numRecieved = mq_receive(other_server, msgBuf, bufSize, NULL);
	if (numRecieved < 0 && errno != EAGAIN) {
		perror("Error receiving! Reason");
		return -1;
	}
	return (int)numRecieved;
}




