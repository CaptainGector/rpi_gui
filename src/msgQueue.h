/*
 * msgQueue.h
 *
 *  Created on: Nov 24, 2020
 *      Author: Zachary Whitlock
 */

#ifndef SRC_MSGQUEUE_H_
#define SRC_MSGQUEUE_H_

#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <mqueue.h>
#include <errno.h>

#define OTHER_MSG_FILE   	"/otherMsgQueue"
#define CLIENT_MSG_FILE 	"/clientMsgQueue"

#define QUEUE_PERMISSIONS 0660
#define MAX_MESSAGES 10
#define MAX_MSG_SIZE 32
#define MSG_BUFFER_SIZE MAX_MSG_SIZE + 10

int createMsgQueues(int client );
int sendToOther(const char *msgBuf, size_t bufSize);
int readFromOther(char *msgBuf, size_t bufSize);
int closeQueues();

mqd_t other_server;
mqd_t client_server;   // queue descriptors

#endif /* SRC_MSGQUEUE_H_ */
