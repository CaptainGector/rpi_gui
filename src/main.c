/*
 * main.c
 *
 *  Created on: Nov 24, 2020
 *      Author: Zachary Whitlock
 *
 * Creates a GTK3 GUI from a glade-generated file, and attaches
 * handlers.
 *
 */
#include <stdint.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include "msgQueue.h"

// Global items of interest
GtkWidget	*lbl_response;
GtkEntry 	*entry_command;
GtkEntry	*entry_value;

GtkTextBuffer *xCommand;


// Fires when Jame's Truong
void Truong_clicked_cb(GtkButton *button, gpointer user_data) {
	printf("Truong BBB button\n");
	char msgBuf[MAX_MSG_SIZE];
	char msgBuf2[MAX_MSG_SIZE];
	char *subStr;

	const gchar *label = gtk_button_get_label(button);
	strcpy(msgBuf, label);
	subStr = strstr(msgBuf, "<VAL>");
	if (subStr != NULL) {

		// Strips the "<VAL>" from the string and places the result in msgbuf2
		strncpy(msgBuf2, msgBuf, subStr-msgBuf);
		msgBuf2[subStr-msgBuf] = 0; // null terminator

		// Adds the content of the value entry into the string
		const gchar *entry_text;
		guint16 entry_size;
		entry_text = gtk_entry_get_text(entry_value);
		entry_size = gtk_entry_get_text_length(entry_value);
		sprintf(msgBuf, "%s%s", msgBuf2, entry_text);

		printf("Buffer: %s\n", msgBuf);
	}

	// Send the complete command, or simply the label

	if (sendToOther(msgBuf, MAX_MSG_SIZE) != 0) {
		printf("Failed to pass command along!");
	}
}

// Fires when an actuator button is pressed
void Actuator_clicked_cb(GtkButton *button, gpointer user_data) {
	printf("Actuator button clicked\n");
	char msgBuf[MAX_MSG_SIZE];
	char msgBuf2[MAX_MSG_SIZE];
	char *subStr;

	const gchar *label = gtk_button_get_label(button);
	strcpy(msgBuf, label);
	subStr = strstr(msgBuf, "<VAL>");
	if (subStr != NULL) {

		// Strips the "<VAL>" from the string and places the result in msgbuf2
		strncpy(msgBuf2, msgBuf, subStr-msgBuf);
		msgBuf2[subStr-msgBuf] = 0; // null terminator

		// Adds the content of the value entry into the string
		const gchar *entry_text;
		guint16 entry_size;
		entry_text = gtk_entry_get_text(entry_value);
		entry_size = gtk_entry_get_text_length(entry_value);
		sprintf(msgBuf, "%s%s", msgBuf2, entry_text);

		printf("Buffer: %s\n", msgBuf);
	}

	// Send the complete command
	if (sendToOther(msgBuf, MAX_MSG_SIZE) != 0) {
		printf("Failed to pass command along!");
	}
}

// Fires when a sensor button is pressed
void Sensor_clicked_cb(GtkButton *button, gpointer user_data) {
	printf("Sensor button clicked\n");
	char msgBuf[MAX_MSG_SIZE];

	const gchar *label = gtk_button_get_label(button);
	strcpy(msgBuf, label);
	if (sendToOther(msgBuf, MAX_MSG_SIZE) != 0) {
		printf("Failed to pass command along!");
	}
}

// Send button was pressed
void Send_clicked_cb(GtkButton *button, gpointer user_data) {
	printf("Send button clicked\n");
	// Take the contents of the custom command input
	// and send it to the message queue service
	char msgBuf[MAX_MSG_SIZE] = {};

	const gchar *entry_text;
	guint16 entry_size;
	entry_text = gtk_entry_get_text(entry_command);
	entry_size = gtk_entry_get_text_length(entry_command);

	strcpy(msgBuf, entry_text);

	printf("Got text of size %d: %s\n", entry_size, msgBuf);
	if (sendToOther(msgBuf, MAX_MSG_SIZE) != 0) {
		printf("Failed to pass command along!");
	}
}


// Runs repeatedly on a timer
int timer_handler() {
	char msgBuf[MAX_MSG_SIZE] = {};
	char responseBuf[MAX_MSG_SIZE] = {};

	// Show any data we got in the queue
	if (readFromOther(msgBuf, MAX_MSG_SIZE) > 0) {
		printf("Received: %s\n", msgBuf);
		sprintf(responseBuf, "'%s'", msgBuf);
		gtk_label_set_text(GTK_LABEL(lbl_response), responseBuf);    // update label
		return 1;
	} else {
		memset(msgBuf, 0, MAX_MSG_SIZE);
	}

	return 1; // returning 0 will destroy us.
}


int main(int argc, char *argv[]) {
	// Setup message queuing
	if (createMsgQueues(0) < 0) {
		printf("Error creating message queue!\n");
	}

	GtkBuilder 	*builder;
	GtkWidget 	*window;
	GError		*error = NULL;


	gtk_init(&argc, &argv);

	builder = gtk_builder_new();

	// try and load file for GUI
	if (!gtk_builder_add_from_file(builder, "assets/main.glade", &error)){
		g_warning("%s", error->message);
		g_free(error);
		return(1);
	}

	// get widgets/objects
	window = GTK_WIDGET(gtk_builder_get_object(builder, "myWindow"));
	lbl_response = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_response"));
	entry_command = GTK_ENTRY(gtk_builder_get_object(builder, "custom_command"));
	entry_value = GTK_ENTRY(gtk_builder_get_object(builder, "enter_value_field"));
	xCommand = GTK_TEXT_BUFFER(gtk_builder_get_object(builder, "readT_cmd"));

	// Hookup our signals
	gtk_builder_connect_signals(builder, NULL);

	GtkTextBuffer test;
	gtk_builder_connect_signals(builder, &test);

	// Destroy the builder, now that we're done with it.
	g_object_unref(G_OBJECT(builder));

	// Make quitting the GUI also stop the program.
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	// Number is in milliseconds
	g_timeout_add(50, (GSourceFunc)timer_handler, NULL);

	gtk_widget_show(window);

	gtk_main();

	closeQueues();

	return 0;
}

